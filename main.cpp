#include <iostream>
#include <string>
#include <sys/stat.h>
#include <fstream>
#include "include/picosha2.h"

inline bool fileExists (const std::string& name) {
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}

void verifyPassword() {
  std::cout << "Please enter the password: ";
  std::string passwordInput;
  std::getline(std::cin, passwordInput);  
  std::string hash_hex_str = picosha2::hash256_hex_string(passwordInput);
  std::ifstream dataFile ("passwordData");
  std::string password;
  std::getline(dataFile, password);
  if (!(password == hash_hex_str)){
    exit(EXIT_FAILURE);
  }
}

int main(){
  if (!fileExists("passwordData")) {
    std::cout << "You need to set a password first \nEnter a password: "; 
    std::string passwordInput;
    std::getline(std::cin, passwordInput);
    
    std::ofstream dataFile("passwordData");
    std::string hash_hex_str = picosha2::hash256_hex_string(passwordInput);
    dataFile << hash_hex_str;
    dataFile.close();
  }
  verifyPassword();
  std::cout << "Password is valid" << std::endl;
}